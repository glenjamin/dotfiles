{:user
 {:plugins [[lein-ancient "0.6.7"
             :exclusions [org.clojure/clojure org.clojure/tools.reader]]
            [com.jakemccrary/lein-test-refresh "0.18.1"]]
  :dependencies [[org.clojure/tools.nrepl "0.2.12" :exclusions [org.clojure/clojure]]
                 [org.clojure/data.json "0.2.6" :exclusions [org.clojure/clojure]]
                 [spyscope "0.1.4" :exclusions [org.clojure/clojure]]
                 [clj-time "0.9.0" :exclusions [org.clojure/clojure]] ; bump spyscope
                 [clojure-complete "0.2.4" :exclusions [org.clojure/clojure]]
                 [org.clojure/tools.namespace "0.2.11" :exclusions [org.clojure/clojure]]
                 [org.clojure/tools.reader "1.0.0-alpha1" :exclusions [org.clojure/clojure]]
                 [io.aviso/pretty "0.1.18" :exclusions [org.clojure/clojure]]
                 [alembic "0.3.2" :exclusions [org.clojure/clojure]]
                 [im.chit/vinyasa "0.4.2" :exclusions [org.clojure/clojure]]]
  :injections
  [(require 'spyscope.core)
   (require 'vinyasa.inject)
   (require 'io.aviso.repl)
   (vinyasa.inject/in ;; the default injected namespace is `.`
    [alembic.still distill]
    [clojure.tools.namespace.repl refresh]
    [clojure.pprint pprint])]}
 :lt
 {:dependencies [[lein-light-nrepl "0.2.0"]]
  :repl-options
  {:nrepl-middleware [lighttable.nrepl.handler/lighttable-ops]}}}
